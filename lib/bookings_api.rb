require "bookings_api/version"

module BookingsApi
  MOCK_WORKSHOPS = [
    { id: 5, name: "Primer taller", address: "Calle 123", receptionists: [{ id: 10, name: "Pedro Pérez", avatar_url: "https://xtaller-dev.s3.us-east-2.amazonaws.com/sample_receptionist1.png" }], scheduling_unavailabilities: [{ starts_at: Date.today + 3, ends_at: Date.today + 6 }] },
    { id: 6, name: "Segundo taller", address: "Avenida 456", receptionists: [{ id: 11, name: "Juan Gómez", avatar_url: "https://xtaller-dev.s3.us-east-2.amazonaws.com/sample_receptionist2.png" }], scheduling_unavailabilities: [] },
  ]
  BOOKING_FIELDS = { name: "Nombre", email: "Email", phone: "Teléfono", brand: "Marca", model: "Modelo", date: "Fecha", registration: "Matrícula",
    receptionist_id: "Recepcionista", workshop_id: "Taller", requester_ip: "IP" }
  class UserError < StandardError; end
  class ServerError < StandardError; end
  mattr_accessor :api_enabled, :api_key, :api_url, :brand_name

  @@workshops = []
  @@workshops_last_fetch = nil
  
  # Method to be optionally called on server initialization to configure this gem
  def self.setup
    yield self
    raise "Expected 'api_enabled' to be a boolean." unless self.api_enabled.in? [true, false]
    raise "Expected 'api_key' to be a string." unless self.api_key.is_a? String
    raise "Expected 'api_url' to be a string." unless self.api_url.is_a? String
    raise "Expected 'brand_name' to be a string." unless self.brand_name.is_a? String
  end

  def self.workshops
    update_workshops if @@workshops_last_fetch.nil? || @@workshops_last_fetch < 1.hour.ago
    @@workshops
  end

  def self.update_workshops
    if self.api_enabled
      response = send_request(method: :get, path: "/workshops", query: { brand: self.brand_name })
      response.each do |w|
        raise ServerError, "Missing required attribute in workshop: #{w.inspect}" if [:id, :name, :address, :receptionists].any?{ |a| w[a].nil? }
        raise ServerError, "Missing required attribute in receptionist: #{w.inspect}" if w[:receptionists].any?{|r| [:id, :name].any?{ |a| r[a].nil? }}
        w[:scheduling_unavailabilities].each do |s|
          s[:starts_at] = s[:starts_at].to_date
          s[:ends_at] = s[:ends_at].to_date
        end
      end
    else
      response = MOCK_WORKSHOPS
    end
    @@workshops = response
  rescue => e
    Notifier.exception e
  end

  # Sends a preorder to the API. Raises a PreorderError if there's any problem contacting the API.
  def self.send_booking(booking)
    raise ServerError, "Missing parameter" if booking.blank?
    booking = booking.to_hash.symbolize_keys.slice(*BOOKING_FIELDS.keys)
    BOOKING_FIELDS.each do |key, pretty_name|
      raise UserError, "Campo faltante: #{pretty_name}" if booking[key].blank?
      raise UserError, "Campo demasiado largo: #{pretty_name}" if booking[key].length > 64
    end
    booking[:workshop_id] = booking[:workshop_id].to_i
    booking[:receptionist_id] = booking[:receptionist_id].to_i
    begin
      date = booking[:date].respond_to?('cwday') ? booking[:date] : Date.parse(booking[:date])
      raise UserError, "No es posible agendar los fines de semana." if date.cwday.in? [6, 7]
    rescue ArgumentError
      raise UserError, "La fecha no es válida (#{booking[:date]})."
    end
    raise UserError, "La fecha debe ser al menos dentro de dos días." unless date >= 2.days.from_now
    workshop = workshops.find{|w| w[:id] == booking[:workshop_id]}
    raise UserError, "El taller seleccionado no existe (#{booking[:workshop_id]})." unless workshop.present?
    unavailability = workshop[:scheduling_unavailabilities].find{|u|
      u[:starts_at] <= date && u[:ends_at] >= date
    }
    raise UserError, "No es posible agendarse entre el #{unavailability[:starts_at].strftime("%d/%m/%Y")} y #{unavailability[:ends_at].strftime("%d/%m/%Y")}." if unavailability.present?
    raise UserError, "El recepcionista seleccionado no existe (#{booking[:receptionist_id]})" unless workshops.any?{|w| w[:receptionists].any?{|r| r[:id] == booking[:receptionist_id]}}
    send_request(
      method: :post,
      path: "/bookings",
      body: booking
    )
  end

  private

  # Method to handle all HTTP requests to the API
  def self.send_request(method:, path:, query: nil, body: nil, json: true)
    return nil unless self.api_enabled
    begin
      response = HTTParty.public_send(method, "#{self.api_url}#{path}", {
        headers: {
          'Authorization': "Bearer #{self.api_key}"
        },
        query: query,
        body: body
      })
    rescue => e
      puts "ApiError: #{e.message}"
      raise ServerError, "An unexpected error occurred when connecting to the API."
    end
    unless response.success?
      puts "Received error response from API: #{response.body}"
      begin
        response_json = JSON.parse(response.body, symbolize_names: true)
        raise ServerError, response_json[:message].presence || "API responded with status code #{response.code} and no error message."
      rescue JSON::ParserError => e
        raise ServerError, "API responded with status code #{response.code}."
      end
    end
    if json
      response_json = JSON.parse(response.body, symbolize_names: true)
      puts "Received JSON response from API: #{response_json}"
      return response_json
    else
      puts "Received response from API with length #{response.length}."
      return response
    end
  end
end
