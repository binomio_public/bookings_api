
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "bookings_api/version"

Gem::Specification.new do |spec|
  spec.name          = "bookings_api"
  spec.version       = BookingsApi::VERSION
  spec.authors       = ["Bruno"]
  spec.email         = ["bruno.giorello@binomio.uy"]

  spec.summary       = "XTaller bookings API connector"
  spec.description   = "A gem to connect Fiancar sites with the XTaller system."
  spec.homepage      = "http://binomio.uy"

  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "localhost"
    spec.metadata["homepage_uri"] = spec.homepage
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files = Dir["lib/**/*", "app/**/*"]
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
end
